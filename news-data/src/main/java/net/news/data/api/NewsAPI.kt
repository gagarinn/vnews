package net.news.data.api

import net.news.domain.model.Kernel
import net.news.domain.model.KeyWord
import net.news.domain.model.NewsEvent
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Burmaka V on 10.04.2020.
 */
interface NewsAPI {

    @GET("/kernel_list.json")
    fun getKernels(): Call<List<Kernel>>

    @GET("/api/key/list")
    fun getKeyWords(@Query("kernelIdentifier") kernelId: String): Call<List<KeyWord>>

    @GET("/api/event/list")
    fun getNewsEvents(@Query("kernelIdentifier") kernelId: String): Call<List<NewsEvent>>
}