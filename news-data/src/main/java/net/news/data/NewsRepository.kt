package net.news.data

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext
import net.core.domain.ResultObj
import net.news.data.api.NewsAPI
import net.news.domain.model.Kernel
import net.news.domain.model.KeyWord
import net.news.domain.model.NewsEvent
import retrofit2.Call
import java.io.IOException
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by Burmaka V on 11.04.2020.
 */
@Singleton
class NewsRepository @Inject constructor(
    private val api: NewsAPI
) {

    fun getKernels(): Flow<ResultObj<List<Kernel>>> = flow {
        emit(api.getKernels().runRequest())
    }

//    fun getNewsEvents(kernelId: String): Flow<ResultObj<List<NewsEvent>>> = flow {
//        emit(api.getNewsEvents(kernelId).runRequest())
//    }
    suspend fun getNewsEvents(kernelId: String): ResultObj<List<NewsEvent>> = api.getNewsEvents(kernelId).runRequest()

    suspend fun getKeyWords(kernelId: String): ResultObj<List<KeyWord>> = api.getKeyWords(kernelId).runRequest()

//    fun getKeyWords(kernelId: String): Flow<ResultObj<List<KeyWord>>> = flow {
//        emit(api.getKeyWords(kernelId).runRequest())
//    }

    private suspend fun <T : Any> Call<T>.runRequest(): ResultObj<T> =
        withContext(Dispatchers.IO) {
            try {
                val response = execute()
                when {
                    response.isSuccessful -> response.body()?.run {
                        ResultObj.Success(this)
                    } ?: ResultObj.Error(NullPointerException())
                    else -> {
                        ResultObj.Error(Exception())
                    }
                }
            } catch (e: IOException) {
                ResultObj.Error(e)
            }
        }
}