package net.news.data.di

import dagger.Binds
import dagger.Module
import dagger.Provides
import net.news.data.api.NewsAPI
import net.news.data.usecaseimpl.GetKernelsUseCaseImpl
import net.news.data.usecaseimpl.GetKeyWordsUseCaseImpl
import net.news.data.usecaseimpl.GetNewsEventsUseCaseImpl
import net.news.domain.usecase.GetKernelsUseCase
import net.news.domain.usecase.GetKeyWordsUseCase
import net.news.domain.usecase.GetNewsEventsUseCase
import retrofit2.Retrofit

/**
 * Created by Burmaka V on 12.04.2020.
 */
@Module(includes = [UseCasesModule::class, ApiModule::class])
interface NewsDataModule

@Module
private interface UseCasesModule {

    @Binds
    fun bindGetKernelsUseCase(
        getGetKernelsUseCase: GetKernelsUseCaseImpl
    ): GetKernelsUseCase

    @Binds
    fun bindGetCategoriesUseCase(
        getCategoriesUseCaseImpl: GetKeyWordsUseCaseImpl
    ): GetKeyWordsUseCase

    @Binds
    fun bindGetNewsUseCase(
        getGetNewsUseCaseImpl: GetNewsEventsUseCaseImpl
    ): GetNewsEventsUseCase
}

@Module
private object ApiModule {

    @Provides
    fun provideApi(retrofit: Retrofit): NewsAPI {
        return retrofit.create(NewsAPI::class.java)
    }
}