package net.news.data.usecaseimpl

import android.content.Context
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import net.core.domain.ResultObj
import net.news.data.NewsRepository
import net.news.domain.model.Kernel
import net.news.domain.usecase.GetKernelsUseCase
import javax.inject.Inject
import org.json.JSONException
import org.json.JSONObject
import org.json.JSONArray
import java.io.IOException


/**
 * Created by Burmaka V on 09.04.2020.
 */
class GetKernelsUseCaseImpl @Inject constructor(
    private val repository: NewsRepository,
    private val context: Context
) : GetKernelsUseCase {

    override fun execute(): Flow<ResultObj<List<Kernel>>> {
//        return repository.getKernels()
        return getMockedKernels()
    }

    private fun getMockedKernels(): Flow<ResultObj<List<Kernel>>> {
        try {
            val resultList = mutableListOf<Kernel>()
            val jsonLocation =
                context.assets.open("kernel_list.json").bufferedReader().use { it.readText() }
            val jsonobject = JSONArray(jsonLocation)

            for (i in 0 until jsonobject.length()) {
                val jb = jsonobject.get(i) as JSONObject
                resultList.add(
                    Kernel(
                        _id = jb.getString("_id"),
                        date = jb.getString("date"),
                        __v = jb.getString("__v")
                    )
                )
            }
            return flow {
                emit(ResultObj.Success<List<Kernel>>(resultList))
            }
        } catch (e: IOException) {
            e.printStackTrace()
            return flow {
                emit(ResultObj.Error(e))
            }
        } catch (e: JSONException) {
            e.printStackTrace()
            return flow {
                emit(ResultObj.Error(e))
            }
        }
    }
}