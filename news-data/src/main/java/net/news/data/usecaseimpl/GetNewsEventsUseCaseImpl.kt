package net.news.data.usecaseimpl

import kotlinx.coroutines.flow.Flow
import net.core.domain.ResultObj
import net.news.data.NewsRepository
import net.news.domain.model.NewsEvent
import net.news.domain.usecase.GetNewsEventsUseCase
import javax.inject.Inject

/**
 * Created by Burmaka V on 09.04.2020.
 */
 class GetNewsEventsUseCaseImpl @Inject constructor(
    private val repository: NewsRepository
): GetNewsEventsUseCase {

    override suspend fun  execute(kernelId: String):
            ResultObj<List<NewsEvent>> {
        return repository.getNewsEvents(kernelId)
//    override fun execute(kernelId: String):
//            Flow<ResultObj<List<NewsEvent>>> {
//        return repository.getNewsEvents(kernelId)
//        return  ResultObj.Success(
//                    arrayListOf(
//                        NewsEvent(_id = "000", date = "2020-04-01T16:10:09.000Z", hash = "hash", text = "some text", uri = ""),
//                        NewsEvent(_id = "111", date = "2020-04-02T16:10:09.000Z", hash = "hash2", text = "some text2", uri = ""),
//                        NewsEvent(_id = "222", date = "2020-04-03T16:10:09.000Z", hash = "hash2", text = "some text2", uri = ""),
//                        NewsEvent(_id = "333", date = "2020-04-04T16:10:09.000Z", hash = "hash2", text = "some text2", uri = ""),
//                        NewsEvent(_id = "444", date = "2020-04-05T16:10:09.000Z", hash = "hash2", text = "some text2", uri = ""),
//                        NewsEvent(_id = "555", date = "2020-04-06T16:10:09.000Z", hash = "hash2", text = "some text2", uri = ""),
//                        NewsEvent(_id = "666", date = "2020-04-07T16:10:09.000Z", hash = "hash2", text = "some text2", uri = ""),
//                        NewsEvent(_id = "777", date = "2020-04-08T16:10:09.000Z", hash = "hash2", text = "some text2", uri = ""),
//                        NewsEvent(_id = "888", date = "2020-04-09T16:10:09.000Z", hash = "hash2", text = "some text2", uri = ""),
//                        NewsEvent(_id = "001", date = "2020-04-10T16:10:09.000Z", hash = "hash2", text = "some text2", uri = ""),
//                        NewsEvent(_id = "002", date = "2020-04-10T16:10:09.000Z", hash = "hash2", text = "some text2", uri = ""),
//                        NewsEvent(_id = "003", date = "2020-04-10T16:10:09.000Z", hash = "hash2", text = "some text2", uri = "")
//                    )
//                )
    }
}