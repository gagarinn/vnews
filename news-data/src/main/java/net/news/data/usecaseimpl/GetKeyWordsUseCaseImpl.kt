package net.news.data.usecaseimpl

import kotlinx.coroutines.flow.Flow
import net.core.domain.ResultObj
import net.news.data.NewsRepository
import net.news.domain.model.KeyWord
import net.news.domain.usecase.GetKeyWordsUseCase
import javax.inject.Inject

/**
 * Created by Burmaka V on 09.04.2020.
 */
class GetKeyWordsUseCaseImpl @Inject constructor(
    private val repository: NewsRepository
) : GetKeyWordsUseCase {

    override suspend fun execute(kernelId: String): ResultObj<List<KeyWord>> {
        return repository.getKeyWords(kernelId)
    }
}