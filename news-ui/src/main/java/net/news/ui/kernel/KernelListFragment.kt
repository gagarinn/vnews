package net.news.ui.kernel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import net.core.ui.BaseFragment
import net.core.ui.showSnackbarMessage
import net.news.domain.model.Kernel
import net.news.ui.databinding.FragmentKernelListBinding

class KernelListFragment : BaseFragment() {

    companion object {
        fun newInstance() = KernelListFragment()
    }

    val viewModel: KernelListViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[KernelListViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentKernelListBinding.inflate(inflater)
            .also {
                it.viewModel = viewModel
                it.lifecycleOwner = viewLifecycleOwner
            }
        viewModel.events.observe(viewLifecycleOwner, Observer { handleEvent(it) })
        lifecycle.addObserver(viewModel)
        return binding.root
    }

    private fun handleEvent(event: KernelListViewModel.UiEvent?) {
        when (event) {
            is KernelListViewModel.UiEvent.OnItemClicked -> navigateToDetails(event.kernel)
            is KernelListViewModel.UiEvent.Error -> showSnackbarMessage(event.message)
        }
    }

    private fun navigateToDetails(kernel: Kernel) {
        val kernelId = kernel._id
        if (kernelId.isEmpty()) {
            showSnackbarMessage("todo error message")
            return
        }
        findNavController().navigate(
            KernelListFragmentDirections.actionKernelsListToEventsList(
                kernelId
            )
        )
    }

    override fun onDestroyView() {
        lifecycle.removeObserver(viewModel)
        super.onDestroyView()
    }
}
