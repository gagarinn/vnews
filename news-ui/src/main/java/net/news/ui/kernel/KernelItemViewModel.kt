package net.news.ui.kernel

import net.news.domain.model.Kernel

/**
 * Created by Burmaka V on 14.04.2020.
 */
class KernelItemViewModel(
    val index: String,
    val kernel: Kernel,
    private val callBack: ((kernel: Kernel) -> Unit)? = null
) {
    fun onClick() {
        callBack?.invoke(kernel)
    }
}