package net.news.ui.kernel.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import net.core.ui.MainActivityViewModel
import net.core.ui.ToolbarHandler
import net.core.ui.di.PerFragment
import net.core.ui.di.ViewModelKey
import net.news.ui.kernel.KernelListFragment
import net.news.ui.kernel.KernelListViewModel

/**
 * Created by Burmaka V on 12.04.2020.
 */
@Module
interface KernelUiModule {
    @ContributesAndroidInjector(modules = [KernelListViewModelModule::class, ToolbarHandlerModule::class])
    @PerFragment
    fun providesNewsListFragment(): KernelListFragment
}

@Module
internal interface KernelListViewModelModule {

    @PerFragment
    @Binds
    @IntoMap
    @ViewModelKey(KernelListViewModel::class)
    fun postKernelListViewModel(viewModel: KernelListViewModel): ViewModel
}

@Module
internal class ToolbarHandlerModule {
    @PerFragment
    @Provides
    fun provideToolbarHandler(
        fragment: KernelListFragment
    ): ToolbarHandler? {
        return fragment.activity?.run { ViewModelProviders.of(this)[MainActivityViewModel::class.java] }
    }
}