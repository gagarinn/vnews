package net.news.ui.kernel

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import net.core.domain.ResultObj
import net.core.ui.RecyclerViewItem
import net.core.ui.SingleLiveEvent
import net.core.ui.ToolbarHandler
import net.news.domain.model.Kernel
import net.news.domain.usecase.GetKernelsUseCase
import net.news.ui.R
import javax.inject.Inject

class KernelListViewModel @Inject constructor(
    private val getKernelUseCase: GetKernelsUseCase,
    private val toolbarHandler: ToolbarHandler?
) : ViewModel(), LifecycleObserver {

    val isLoading = ObservableBoolean()
    val recyclerItems = ObservableField<List<RecyclerViewItem>>()

    private val _events = SingleLiveEvent<UiEvent>()
    val events: LiveData<UiEvent>
        get() = _events

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        toolbarHandler?.setTitleResId(R.string.kernel_fragment)
        toolbarHandler?.setSubTitle("Total items: " + recyclerItems.get()?.size)
        if (recyclerItems.get().isNullOrEmpty()) {
            refresh()
        }
    }

    fun onRefreshListener() {
        refresh()
    }

    @ExperimentalCoroutinesApi
    private fun refresh() {
        isLoading.set(true)
        viewModelScope.launch {
            try {
                getKernelUseCase.execute().collect { result ->
                    when (result) {
                        is ResultObj.Success -> onSuccess(result.data)
                        is ResultObj.Error -> onError(result.exception)
                    }
                }
            } catch (e: Exception) {
                onError(e)
            }
        }
    }

    private fun onSuccess(newsEvents: List<Kernel>) {
        isLoading.set(false)
        if (newsEvents.isNotEmpty()) {
            val kernels = newsEvents.mapIndexed { index: Int, kernel: Kernel ->
                createKernelItemViewModel(
                    index,
                    kernel
                )
            }
                .map { it.toRecyclerViewItem() }
            recyclerItems.set(kernels)
        }
        toolbarHandler?.setSubTitle("Total items: " + recyclerItems.get()?.size)
    }

    private fun onError(e: Throwable) {
        isLoading.set(false)
        toolbarHandler?.setSubTitle("Total items: " + recyclerItems.get()?.size)
        Log.e("NewsListViewModel", e.message)
        _events.value = UiEvent.Error(e.message ?: "Unknown error")
    }

    private fun createKernelItemViewModel(index: Int, kernel: Kernel) =
        KernelItemViewModel(index.toString(), kernel) { onItemClicked(it) }

    private fun KernelItemViewModel.toRecyclerViewItem(): RecyclerViewItem {
        return RecyclerViewItem(
            this,
            R.layout.item_kernel
        )
    }

    private fun onItemClicked(kernel: Kernel) {
        _events.value = UiEvent.OnItemClicked(kernel)
    }

    sealed class UiEvent {
        data class Error(val message: String) : UiEvent()
        data class OnItemClicked(val kernel: Kernel) : UiEvent()
    }
}
