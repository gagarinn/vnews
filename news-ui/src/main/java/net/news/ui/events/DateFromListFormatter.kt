package net.news.ui.events

import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.formatter.ValueFormatter

/**
 * Created by Burmaka V on 21.04.2020.
 */
class DateFromListFormatter (
    private val chart: LineChart,
    private val dateList: List<String>
) : ValueFormatter() {
    override fun getFormattedValue(value: Float): String {
        return dateList[value.toInt()]
    }
}