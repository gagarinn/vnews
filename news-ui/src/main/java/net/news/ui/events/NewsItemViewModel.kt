package net.news.ui.events

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import net.core.ui.convertDate
import net.news.domain.model.NewsEvent

/**
 * Created by Burmaka V on 11.04.2020.
 */
class NewsItemViewModel(
    val index: String,
    val newsEvent: NewsEvent,
    private var callBack: ((newsEvent: NewsEvent) -> Unit)? = null
) : ViewModel() {
    val date = ObservableField<String>()
    val title = ObservableField<String>()
    val imageUrl = ObservableField<String>()
    val imageLoading = ObservableBoolean()

    init {
        title.set(if (newsEvent.text.isNullOrEmpty()) "empty" else newsEvent.text)
        date.set(if (newsEvent.date.isNullOrEmpty()) "empty" else newsEvent.date.convertDate())
        newsEvent.metaData?.let {
            imageUrl.set(it.imgUrl)
        }
    }

    fun onClick() {
        callBack?.invoke(newsEvent)
    }

    override fun onCleared() {
        super.onCleared()
        callBack = null
    }
}