package net.news.ui.events


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import net.core.ui.BaseFragment
import net.core.ui.showSnackbarMessage
import net.news.ui.databinding.FragmentBarChartBinding
import java.util.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class BarChartFragment : BaseFragment() {


    lateinit var binding: FragmentBarChartBinding

    val viewModel: NewsEventsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[NewsEventsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentBarChartBinding.inflate(inflater)
            .also {
                it.viewModel = viewModel
                it.lifecycleOwner = viewLifecycleOwner
            }
        viewModel.events.observe(viewLifecycleOwner, Observer { handleEvent(it) })
        lifecycle.addObserver(viewModel)
        initChart()
        return binding.root
    }

    private fun initChart() {
        binding.chart?.apply {

            this.setDrawBarShadow(false)
            this.setDrawValueAboveBar(true)

            this.description.isEnabled = false

            // if more than 60 entries are displayed in the chart, no values will be
            // drawn
            this.setMaxVisibleValueCount(60)

            // scaling can now only be done on x- and y-axis separately
            this.setPinchZoom(false)

            this.setDrawGridBackground(false)
            // chart.setDrawYLabels(false);

            val xAxisFormatter = DayAxisValueFormatter(this)

            val xAxis = this.xAxis
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.setDrawGridLines(false)
            xAxis.granularity = 1f // only intervals of 1 day
            xAxis.labelCount = 7
            xAxis.valueFormatter = xAxisFormatter

            val custom = MyValueFormatter("$")

            val leftAxis = this.axisLeft
            leftAxis.setLabelCount(8, false)
            leftAxis.valueFormatter = custom
            leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
            leftAxis.spaceTop = 15f
            leftAxis.axisMinimum = 0f // this replaces setStartAtZero(true)

            val rightAxis = this.axisRight
            rightAxis.setDrawGridLines(false)
            rightAxis.setLabelCount(8, false)
            rightAxis.valueFormatter = custom
            rightAxis.spaceTop = 15f
            rightAxis.axisMinimum = 0f // this replaces setStartAtZero(true)
        }
    }

    private fun handleEvent(event: NewsEventsViewModel.UiEvent?) {
        when (event) {
            is NewsEventsViewModel.UiEvent.OnDataRecived -> plotData(event.plotObject)
            is NewsEventsViewModel.UiEvent.Error -> showSnackbarMessage(event.message)
        }
    }

    private fun plotData(plotObject: NewsEventsViewModel.PlotObject) {
        // add data
        binding.seekBar.setProgress(12)
        setData(plotObject.events)
        binding.chart.invalidate()

//        // draw points over time
        binding.chart.animateX(1500)
    }

    private fun setData(newsEvents: List<Int>, count: Int = 12, range: Float = 50.0f) {

        val start = 0f

        val values = ArrayList<BarEntry>()

        var i = start.toInt()
        while (i < newsEvents.size) {
            values.add(BarEntry(i.toFloat(), newsEvents[i].toFloat()))
            i++
        }

        val set1: BarDataSet

        if (binding.chart.data != null && binding.chart.data.dataSetCount > 0) {
            set1 = binding.chart.data.getDataSetByIndex(0) as BarDataSet
            set1.values = values
            binding.chart.data.notifyDataChanged()
            binding.chart.notifyDataSetChanged()

        } else {
            set1 = BarDataSet(values, "The year 2017")

            set1.setDrawIcons(false)

            val dataSets = ArrayList<IBarDataSet>()
            dataSets.add(set1)

            val data = BarData(dataSets)
            data.setValueTextSize(10f)
            data.barWidth = 0.9f

            binding.chart.data = data
        }
    }
}

