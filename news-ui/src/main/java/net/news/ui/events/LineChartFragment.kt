package net.news.ui.events


import android.content.Context
import android.graphics.Color
import android.graphics.DashPathEffect
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.LimitLine
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.interfaces.dataprovider.LineDataProvider
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.utils.Utils
import net.core.ui.BaseFragment
import net.core.ui.showSnackbarMessage
import net.news.ui.R
import net.news.ui.databinding.FragmentLineChartBinding
import java.util.ArrayList

/**
 * A simple [Fragment] subclass.
 */
class LineChartFragment : BaseFragment(), SeekBar.OnSeekBarChangeListener {

    lateinit var binding: FragmentLineChartBinding

    var plotObject: NewsEventsViewModel.PlotObject? = null

    val viewModel: NewsEventsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[NewsEventsViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLineChartBinding.inflate(inflater)
            .also {
                it.viewModel = viewModel
                it.lifecycleOwner = viewLifecycleOwner
            }
        binding.seekBar.setOnSeekBarChangeListener(this)
        viewModel.events.observe(viewLifecycleOwner, Observer { handleEvent(it) })
        lifecycle.addObserver(viewModel)
        initChart()
        return binding.root
    }

    private fun initChart() {
        binding.chart?.apply {
            // background color
            this.setBackgroundColor(Color.WHITE)

            // disable description text
            this.getDescription().setEnabled(false)

            // enable touch gestures
            this.setTouchEnabled(true)

            // set listeners
//                this.setOnChartValueSelectedListener(this)
            this.setDrawGridBackground(false)

            // enable scaling and dragging
            this.setDragEnabled(true)
            this.setScaleEnabled(true)

            // force pinch zoom along both axis
            this.setPinchZoom(true)
        }
    }

    private fun handleEvent(event: NewsEventsViewModel.UiEvent?) {
        when (event) {
            is NewsEventsViewModel.UiEvent.OnDataRecived -> plotData(event.plotObject)
            is NewsEventsViewModel.UiEvent.Error -> showSnackbarMessage(event.message)
        }
    }

    private fun plotData(plotData: NewsEventsViewModel.PlotObject) {
        plotObject = plotData
        // add data
        binding.seekBar.max = plotData.events.size - 1
        binding.tvMin.text = plotData.eventDates[0]

        setUpAxises(plotData.eventDates)

        val daysInYear = 365
        val defaultRangeToShow =
            if (plotData.events.size <= daysInYear) plotData.events.size else daysInYear
        binding.tvMax.text = plotData.eventDates[defaultRangeToShow]
        binding.seekBar.progress = defaultRangeToShow
        setData(defaultRangeToShow)
        // draw points over time
        binding.chart.animateX(1000)
    }

    private fun setUpAxises(eventDates: List<String>) {
        binding.chart?.apply {
            val xAxisFormatter = DateFromListFormatter(this, eventDates)
            // // X-Axis Style // //
            val xAxis: XAxis = this.xAxis
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.granularity = 1f
            xAxis.valueFormatter = xAxisFormatter
            // vertical grid lines
            xAxis.enableGridDashedLine(10f, 10f, 0f)

            // // Y-Axis Style // //
            val yAxis: YAxis = this.getAxisLeft()
            // disable dual axis (only use LEFT axis)
            this.getAxisRight().setEnabled(false)
            // horizontal grid lines
            yAxis.enableGridDashedLine(10f, 10f, 0f)
        }
    }

    private fun setData(count: Int = 0) {

        val newsEvents = plotObject?.events ?: emptyList()
        val values = ArrayList<Entry>()
        val pointsToPlot = if (count == 0) newsEvents.size - 1 else count

        for (i in 0..pointsToPlot) {
            values.add(
                Entry(
                    i.toFloat(),
                    newsEvents[pointsToPlot - i].toFloat(),
                    resources.getDrawable(R.drawable.star)
                )
            )
        }

        val set1: LineDataSet

        if (binding.chart.getData() != null && binding.chart.getData().getDataSetCount() > 0) {
            set1 = binding.chart.getData().getDataSetByIndex(0) as LineDataSet
            set1.values = values
            set1.notifyDataSetChanged()
            binding.chart.getData().notifyDataChanged()
            binding.chart.notifyDataSetChanged()
        } else {
            // create a dataset and give it a type
            set1 = LineDataSet(values, "DataSet 11")
            set1.setDrawIcons(false)

            // black lines and points
            set1.color = Color.BLACK
            set1.setCircleColor(Color.BLACK)

            // line thickness and point size
            set1.lineWidth = 1f
            set1.circleRadius = 3f

            // draw points as solid circles
            set1.setDrawCircleHole(false)

            // text size of values
            set1.valueTextSize = 9f

            val dataSets = ArrayList<ILineDataSet>()
            dataSets.add(set1) // add the data sets

            // create a data object with the data sets
            val data = LineData(dataSets)

            // set data
            binding.chart.setData(data)
        }
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        plotObject?.let {
            binding.tvMax.text =it.eventDates[binding.seekBar.progress]
        }
        setData(binding.seekBar.progress)
        binding.chart.invalidate()
    }
}

