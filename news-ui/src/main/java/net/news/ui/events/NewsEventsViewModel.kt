package net.news.ui.events

import android.util.Log
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LiveData
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import net.core.domain.ResultObj
import net.core.ui.RecyclerViewItem
import net.core.ui.SingleLiveEvent
import net.core.ui.ToolbarHandler
import net.core.ui.toDate
import net.news.domain.model.NewsEvent
import net.news.domain.usecase.GetNewsEventsUseCase
import net.news.ui.R
import java.util.Date
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Burmaka V on 10.04.2020.
 */
class NewsEventsViewModel @Inject constructor(
    private val getNewsEventsUseCase: GetNewsEventsUseCase,
    private val args: NewsEventsArgumentsBundle,
    private val toolbarHandler: ToolbarHandler?
) : ViewModel(), LifecycleObserver {
    val isLoading = ObservableBoolean()
    val recyclerItems = ObservableField<List<RecyclerViewItem>>()

    private val _events = SingleLiveEvent<UiEvent>()
    val events: LiveData<UiEvent>
        get() = _events

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        toolbarHandler?.setTitleResId(R.string.news_list_fragment)
        toolbarHandler?.setSubTitle("Total items: " + recyclerItems.get()?.size)
        if (recyclerItems.get().isNullOrEmpty()) {
            refresh()
        }
    }

    fun onRefreshListener() {
        refresh()
    }

    @ExperimentalCoroutinesApi
    private fun refresh() {
        isLoading.set(true)
        viewModelScope.launch {
            try {
//                getNewsEventsUseCase.execute(args.kernelId).collect { result ->
//                    when (result) {
//                        is ResultObj.Success -> onSuccess(result.data)
//                        is ResultObj.Error -> onError(result.exception)
//                    }
//                }
                isLoading.set(true)
                val result = getNewsEventsUseCase.execute(args.kernelId)
                    when (result) {
                        is ResultObj.Success -> onSuccess(result.data)
                        is ResultObj.Error -> onError(result.exception)
                    }
            } catch (e: Exception) {
                onError(e)
            }
        }
    }

    private fun onSuccess(newsEvents: List<NewsEvent>) {
        prepareData(newsEvents)
        if (newsEvents.isNotEmpty()) {
            val kernels = newsEvents.mapIndexed { index: Int, newsEvent: NewsEvent ->
                createNewsItemViewModel(
                    index,
                    newsEvent
                )
            }
                .map { it.toRecyclerViewItem() }
            recyclerItems.set(kernels)
        }
        isLoading.set(false)
        toolbarHandler?.setSubTitle("Total items: " + recyclerItems.get()?.size)
    }

    private fun prepareData(newsEvents: List<NewsEvent>) {
        if (newsEvents.isNotEmpty() && newsEvents.size > 1) {

            val beginDate = newsEvents.last().date.toDate()
            val endDate = newsEvents.first().date.toDate()
            val days = getDifferenceDays(beginDate, endDate) + 1 // include first day

            val eventsMap = convertEventsListToMapOfEvents(newsEvents)

            val rangedEvents = MutableList<Int>(size = days)  { index -> 0 }
            val rangedDates = MutableList<String>(size = days)  { index -> "day$index" }

            eventsMap.forEach {(key, value) ->
                val index = getDifferenceDays(newsEvents.last().date.cutDateFromFullDate().toDate("yyyy-MM-dd"), key.toDate("yyyy-MM-dd"))
                rangedEvents[index] = value
             }

            _events.value = UiEvent.OnDataRecived(PlotObject(rangedEvents, rangedDates))
        }
    }

    private fun getDifferenceDays(d1: Date?, d2: Date? ) : Int {
        val diff = (d2?.getTime() ?: 0L) - (d1?.getTime() ?: 0L)
        return  Math.abs(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)).toInt()
    }

    private fun convertEventsListToMapOfEvents(newsEvents: List<NewsEvent>) : Map<String, Int> {
        var currentDay = newsEvents.first().date.cutDateFromFullDate()
        var eventsNumber = 1
        val eventsMap = mutableMapOf<String, Int>()

        var event = newsEvents.first()

        for (i in 1..newsEvents.size - 2) { // todo check indexes
            event = newsEvents[i]
            if (currentDay == event.date.cutDateFromFullDate()) {
                eventsNumber++
            } else {
                eventsMap.put(currentDay, eventsNumber)
                currentDay = event.date.cutDateFromFullDate()
                eventsNumber = 1
            }
        }

        event = newsEvents.last()
        if (currentDay == event.date.cutDateFromFullDate()) {
            eventsNumber++
            eventsMap.put(currentDay, eventsNumber)
        } else {
            eventsMap.put(event.date.cutDateFromFullDate(), 1)
        }
        return eventsMap
    }

    /**
     *  Cut "yyyy-MM-dd" from "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
     */
    private fun String.cutDateFromFullDate() = substring(0, 10)

//    + todo isSameDay "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
//    + todo Set<Date, Number>
//    + todo index of day = getDifferenceDays(first, current)




    private fun onError(e: Throwable) {
        isLoading.set(false)
        toolbarHandler?.setSubTitle("Total items: " + recyclerItems.get()?.size)
        Log.e("NewsListViewModel", e.message)
        _events.value =
            UiEvent.Error(e.message ?: "Unknown error")
    }

    private fun createNewsItemViewModel(index: Int, newsEvent: NewsEvent) =
        NewsItemViewModel(index.toString(), newsEvent) { onItemClicked(it) }

    private fun NewsItemViewModel.toRecyclerViewItem(): RecyclerViewItem {
        return RecyclerViewItem(
            this,
            R.layout.item_news
        )
    }

    override fun onCleared() {
        super.onCleared()
        Log.e("NewsListViewModel", "onCleared items : " + recyclerItems.get()?.size)
        recyclerItems.set(emptyList())
    }

    private fun onItemClicked(newsEvent: NewsEvent) {
        _events.value = UiEvent.OnItemClicked(newsEvent)
    }
    data class PlotObject(val events: List<Int>, val eventDates: List<String>)

    sealed class UiEvent {
        data class Error(val message: String) : UiEvent()
        data class OnItemClicked(val newsEvent: NewsEvent) : UiEvent()
        data class OnDataRecived(val plotObject: PlotObject) : UiEvent()
    }
}

