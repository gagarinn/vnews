package net.news.ui.events

import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import net.core.ui.BaseFragment
import net.core.ui.showSnackbarMessage
import net.news.domain.model.NewsEvent
import net.news.ui.R
import net.news.ui.databinding.FragmentNewsEventsBinding

data class NewsEventsArgumentsBundle(val kernelId: String)

internal fun Bundle.toNewsEventsArgumentsBundle() = NewsEventsArgumentsBundle(
    NewsEventsFragmentArgs.fromBundle(this).kernelId
)

class NewsEventsFragment : BaseFragment() {

    val viewModel: NewsEventsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[NewsEventsViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentNewsEventsBinding.inflate(inflater)
            .also {
                it.viewModel = viewModel
                it.lifecycleOwner = viewLifecycleOwner
            }
        viewModel.events.observe(viewLifecycleOwner, Observer { handleEvent(it) })
        lifecycle.addObserver(viewModel)
        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.settings_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_settings -> {
                navigateToSettings()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleEvent(event: NewsEventsViewModel.UiEvent?) {
        when (event) {
            is NewsEventsViewModel.UiEvent.OnItemClicked -> navigateToDetails(event.newsEvent)
            is NewsEventsViewModel.UiEvent.Error -> showSnackbarMessage(event.message)
        }
    }

    private fun navigateToDetails(newsEvent: NewsEvent) {
        val uri = newsEvent.uri
        if (uri.isEmpty()) {
            showSnackbarMessage("todo error message")
            return
        }
        findNavController().navigate(
            NewsEventsFragmentDirections.actionListNewsToWebView(
                uri
            )
        )
    }

    private fun navigateToSettings() {
//        todo SettingsFragment
    }

    override fun onDestroyView() {
        lifecycle.removeObserver(viewModel)
        super.onDestroyView()
    }
}
