package net.news.ui.events.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import net.core.ui.MainActivityViewModel
import net.core.ui.ToolbarHandler
import net.core.ui.di.PerFragment
import net.core.ui.di.ViewModelKey
import net.news.ui.events.BarChartFragment
import net.news.ui.events.LineChartFragment
import net.news.ui.events.NewsEventsArgumentsBundle
import net.news.ui.events.NewsEventsFragment
import net.news.ui.events.NewsEventsViewModel
import net.news.ui.events.toNewsEventsArgumentsBundle

/**
 * Created by Burmaka V on 12.04.2020.
 */
@Module
interface EventsUiModule {
    @ContributesAndroidInjector(
        modules = [
            NewsListViewModelModule::class,
            NewsEventsArgumentsBundleModule::class
        ]
    )
    @PerFragment
    fun providesNewsListFragment(): NewsEventsFragment

    @ContributesAndroidInjector(
        modules = [
            NewsListViewModelModule::class,
            BarChartFragmentArgumentsBundleModule::class
        ]
    )
    @PerFragment
    fun providesDemoPlotFragment(): BarChartFragment

    @ContributesAndroidInjector(
        modules = [
            NewsListViewModelModule::class,
            LineChartFragmentArgumentsBundleModule::class
        ]
    )
    @PerFragment
    fun providesLineChartFragment(): LineChartFragment
}

@Module
internal class NewsEventsArgumentsBundleModule {
    @PerFragment
    @Provides
    fun provideArguments(fragment: NewsEventsFragment): NewsEventsArgumentsBundle {
        return fragment.arguments?.toNewsEventsArgumentsBundle()
            ?: NewsEventsArgumentsBundle("error get argument")
    }

    @PerFragment
    @Provides
    fun provideNewsEventsFragmentToolbarHandler(
        fragment: NewsEventsFragment
    ): ToolbarHandler? {
        return fragment.activity?.run { ViewModelProviders.of(this)[MainActivityViewModel::class.java] }
    }
}

@Module
internal class BarChartFragmentArgumentsBundleModule {
    @PerFragment
    @Provides
    fun provideArguments(fragment: BarChartFragment): NewsEventsArgumentsBundle {
        return fragment.arguments?.toNewsEventsArgumentsBundle()
            ?: NewsEventsArgumentsBundle("error get argument")
    }

    @PerFragment
    @Provides
    fun provideDemoPlotFragmentToolbarHandler(
        fragment: BarChartFragment
    ): ToolbarHandler? {
        return fragment.activity?.run { ViewModelProviders.of(this)[MainActivityViewModel::class.java] }
    }
}

@Module
internal class LineChartFragmentArgumentsBundleModule {
    @PerFragment
    @Provides
    fun provideArguments(fragment: LineChartFragment): NewsEventsArgumentsBundle {
        return fragment.arguments?.toNewsEventsArgumentsBundle()
            ?: NewsEventsArgumentsBundle("error get argument")
    }

    @PerFragment
    @Provides
    fun provideDemoPlotFragmentToolbarHandler(
        fragment: LineChartFragment
    ): ToolbarHandler? {
        return fragment.activity?.run { ViewModelProviders.of(this)[MainActivityViewModel::class.java] }
    }
}

@Module
internal interface NewsListViewModelModule {
    @PerFragment
    @Binds
    @IntoMap
    @ViewModelKey(NewsEventsViewModel::class)
    fun postListViewModel(viewModel: NewsEventsViewModel): ViewModel
}
