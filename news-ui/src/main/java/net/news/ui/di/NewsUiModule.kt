package net.news.ui.di

import dagger.Module
import net.news.ui.events.di.EventsUiModule
import net.news.ui.kernel.di.KernelUiModule
import net.news.ui.webview.di.WebViewUiModule

/**
 * Created by Burmaka V on 12.04.2020.
 */
@Module(
    includes = [
        EventsUiModule::class,
        KernelUiModule::class,
        WebViewUiModule::class
    ]
)
interface NewsUiModule
