package net.news.ui.webview.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap
import net.core.ui.MainActivityViewModel
import net.core.ui.ToolbarHandler
import net.core.ui.di.PerFragment
import net.core.ui.di.ViewModelKey
import net.news.ui.webview.WebViewFragment
import net.news.ui.webview.WebViewModel

/**
 * Created by Burmaka V on 12.04.2020.
 */
@Module
interface WebViewUiModule {
    @ContributesAndroidInjector(modules = [WebViewModelModule::class, WebViewToolbarHandlerModule::class])
    @PerFragment
    fun providesWebViewFragment(): WebViewFragment
}

@Module
internal interface WebViewModelModule {
    @PerFragment
    @Binds
    @IntoMap
    @ViewModelKey(WebViewModel::class)
    fun postWebViewModel(viewModel: WebViewModel): ViewModel
}

@Module
internal class WebViewToolbarHandlerModule {
    @PerFragment
    @Provides
    fun provideToolbarHandler(
        fragment: WebViewFragment
    ): ToolbarHandler? {
        return fragment.activity?.run { ViewModelProviders.of(this)[MainActivityViewModel::class.java] }
    }
}