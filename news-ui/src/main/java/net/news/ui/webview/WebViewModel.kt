package net.news.ui.webview

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ViewModel
import net.core.ui.ToolbarHandler
import net.news.ui.R
import javax.inject.Inject

/**
 * Created by Burmaka V on 16.04.2020.
 */

class WebViewModel @Inject constructor(
    private val toolbarHandler: ToolbarHandler?
) : ViewModel(), LifecycleObserver {
    val isLoading = ObservableBoolean(true)
    val isError = ObservableBoolean()

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun onCreate() {
        toolbarHandler?.setTitleResId(R.string.web_view_fragment)
    }

    fun showUrl(url: String){
        toolbarHandler?.setSubTitle(url)
    }
}