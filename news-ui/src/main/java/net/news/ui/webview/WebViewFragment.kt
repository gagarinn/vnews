package net.news.ui.webview

import android.annotation.TargetApi
import android.os.Bundle
import android.view.*
import android.webkit.WebViewClient
import net.news.ui.databinding.FragmentWebViewBinding
import android.content.Intent
import android.os.Build
import android.util.Log
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import androidx.lifecycle.ViewModelProviders
import net.core.ui.BaseFragment
import net.news.ui.R

/**
 * Created by Burmaka V on 11.04.2020.
 */
class WebViewFragment : BaseFragment() {

    private var uri: String? = null

    val viewModel: WebViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory)[WebViewModel::class.java]
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        viewModel.isLoading.set(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentWebViewBinding.inflate(inflater)
            .also {
                it.viewModel = viewModel
                it.lifecycleOwner = viewLifecycleOwner
            }
        lifecycle.addObserver(viewModel)

        arguments?.run {
            binding.webView.loadUrl(WebViewFragmentArgs.fromBundle(this).uri)
            viewModel.showUrl(WebViewFragmentArgs.fromBundle(this).uri)
        }
        val webSettings = binding.webView.settings
        webSettings.javaScriptEnabled = true

        binding.webView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                viewModel.isLoading.set(false)
            }

            @TargetApi(Build.VERSION_CODES.M)
            override fun onReceivedError(
                view: WebView?,
                request: WebResourceRequest?,
                error: WebResourceError?
            ) {
                super.onReceivedError(view, request, error)
                Log.e("WebViewFragment", "onReceivedError request: ${request?.url},\n  error: ${error?.description}")
                viewModel.isError.set(true)
                viewModel.isLoading.set(false)
            }
        }

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.share_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_share -> {
                share()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun share() {
        uri?.let {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(Intent.EXTRA_TEXT, it)
            sendIntent.type = "text/plain"
            startActivity(sendIntent)
        }
    }
}