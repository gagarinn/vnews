package net.core.ui

import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.lifecycle.ViewModel
import javax.inject.Inject

/**
 * Created by Burmaka V on 17.04.2020.
 */
class MainActivityViewModel @Inject constructor(): ViewModel(), ToolbarHandler {
    override fun setSubTitle(subtitle: String?) {
        if (subtitle == null){
            toolbarSubTitle.set("")
        } else {
            toolbarSubTitle.set(subtitle)
        }
    }

    override fun setTitleResId(titleId: Int) {
        toolbarTitle.set(titleId)
    }

    val toolbarTitle = ObservableInt()
    val toolbarSubTitle = ObservableField<String>()
}