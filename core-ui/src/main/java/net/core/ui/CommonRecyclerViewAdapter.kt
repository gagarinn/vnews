package net.core.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import java.lang.RuntimeException

/**
 * Created by Burmaka V on 14.04.2020.
 */
private const val DEFAULT_VARIABLE_ID = -1
private const val DEFAULT_VARIABLE_NAME = "viewModel"

data class RecyclerViewItem(
    val viewModel: Any?,
    @LayoutRes val layoutId: Int,
    val viewModelVariableId: Int = DEFAULT_VARIABLE_ID
)

open class AutoViewTypeRecyclerViewAdapter
    : AbstractRecyclerViewAdapter<RecyclerViewItem, BindingViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, layoutId: Int): BindingViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ViewDataBinding = DataBindingUtil.inflate(inflater, layoutId, parent, false)
        val parentBindingLifecycleOwner = DataBindingUtil.findBinding<ViewDataBinding>(parent)
            ?.lifecycleOwner
        return BindingViewHolder(binding, parentBindingLifecycleOwner)
    }

    override fun onBindViewHolder(holder: BindingViewHolder, position: Int) {
        getItem(position)?.let {
            holder.bind(it)
        }
    }

    override fun getViewType(item: RecyclerViewItem): Int = item.layoutId
}

class BindingViewHolder(
    val binding: ViewDataBinding,
    private val parentBindingLifecycleOwner: LifecycleOwner? = null
) : RecyclerView.ViewHolder(binding.root) {

    private var recyclerViewItem: RecyclerViewItem? = null

    fun bind(recyclerViewItem: RecyclerViewItem) {
        if (recyclerViewItem.viewModel == null) return
        setViewModel(recyclerViewItem)
        binding.executePendingBindings()
    }

    internal fun setViewModel(recyclerViewItem: RecyclerViewItem) {
        this.recyclerViewItem = recyclerViewItem

        if (recyclerViewItem.viewModel == null) return

        val variableId = if (recyclerViewItem.viewModelVariableId == DEFAULT_VARIABLE_ID) {
            DatabindingHelper.getBrIdByVariableName(DEFAULT_VARIABLE_NAME)
                ?: throw RuntimeException(
                    "Unable to set '$DEFAULT_VARIABLE_NAME' variable " +
                            "to the layout related to ${binding.javaClass.name}"
                )
        } else {
            recyclerViewItem.viewModelVariableId
        }

        val variableSuccessfullySet = binding.setVariable(variableId, recyclerViewItem.viewModel)

        if (!variableSuccessfullySet) {
            val variableName = DataBindingUtil.convertBrIdToString(variableId)
            throw RuntimeException(
                "Looks like the layout related to ${binding.javaClass.name} " +
                        "does not have '$variableName' variable."
            )
        }

        parentBindingLifecycleOwner?.let { binding.lifecycleOwner = it }
    }
}

object DatabindingHelper {
    private val previouslyFoundMappings = mutableMapOf<String, Int>()

    fun getBrIdByVariableName(variableName: String): Int? {
        previouslyFoundMappings[variableName]?.let { return it }

        // Starting form 1 because BR._all = 0 is not a variable name.
        for (variableId in 1..Integer.MAX_VALUE) {
            val foundVariableName = DataBindingUtil.convertBrIdToString(variableId) ?: return null
            if (foundVariableName == variableName) {
                previouslyFoundMappings[variableName] = variableId
                return variableId
            }
        }
        return null
    }
}

// todo diffUtil
abstract class AbstractRecyclerViewAdapter<ITEM : Any?, VH : RecyclerView.ViewHolder> :
    RecyclerView.Adapter<VH>() {

    protected var items = mutableListOf<ITEM>()

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int): Int {
        return getItem(position)?.let { getViewType(it) } ?: 0
    }

    private fun isValidPosition(position: Int) = position >= 0 && position < items.size

    override fun getItemId(position: Int): Long = getItem(position)?.hashCode()?.toLong() ?: 0

    abstract fun getViewType(item: ITEM): Int

    fun getItem(position: Int): ITEM? {
        return items.getOrNull(position)
    }

    open fun deleteItemAt(position: Int) {
        if (isValidPosition(position)) {
            this.items.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun updateData(newItems: List<ITEM>) {
        this.items.clear()
        this.items.addAll(newItems)
        notifyDataSetChanged()
        return
    }

    fun insertItemAt(position: Int, item: ITEM) {
        if (this.items.size > position) {
            this.items.add(position, item)
        } else {
            this.items.add(item)
        }
        notifyItemInserted(position)
    }
}
