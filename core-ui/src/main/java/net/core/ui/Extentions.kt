package net.core.ui

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Burmaka V on 11.04.2020.
 */

fun Fragment.showSnackbarMessage(message: CharSequence) {
    this.view?.let {
        Snackbar.make(it, message, Snackbar.LENGTH_SHORT).show()
    }
}

object Decorations {
    @JvmStatic
    fun dividerVertical(context: Context): RecyclerView.ItemDecoration {
        return DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
    }
}

private const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

fun String.convertDate(dateFormat: String = DATE_FORMAT): String? {
    val dateFormatter = SimpleDateFormat(dateFormat, Locale.getDefault())
    var dateFormated: Date?
    try {
        dateFormated = dateFormatter.parse(this)
    } catch (e: ParseException) {
        return this
    }

    val format = SimpleDateFormat("HH:mm dd-MM-yyyy", Locale.getDefault())
    return format.format(dateFormated)
}

fun String.toDate(dateFormat: String = DATE_FORMAT): Date? {
    val dateFormatter = SimpleDateFormat(dateFormat, Locale.getDefault())
    var dateFormated: Date? = null
    try {
        dateFormated = dateFormatter.parse(this)
    } catch (e: ParseException) {
       e.printStackTrace()
    }
    return dateFormated
}
