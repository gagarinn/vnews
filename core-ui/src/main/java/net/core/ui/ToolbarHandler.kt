package net.core.ui

/**
 * Created by Burmaka V on 17.04.2020.
 */

interface ToolbarHandler {
    fun setTitleResId(titleId: Int)
    fun setSubTitle(subtitle: String?)
}