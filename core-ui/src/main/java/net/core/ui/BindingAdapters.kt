package net.core.ui

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

/**
 * Created by Burmaka V on 10.04.2020.
 */

@BindingAdapter("glideImage")
fun ImageView.setGlideImage(glideImageUrl: String?) {
    Glide.with(this.context)
        .load(glideImageUrl)
        .placeholder(R.drawable.ic_image_placeholderr)
//        .error(R.drawable.ic_error) // todo implement image search first
        .into(this)
}

@BindingAdapter("android:visibility")
fun setVisibility(v: View, isVisible: Boolean) {
    v.visibility = if (isVisible) View.VISIBLE else View.GONE
}

@BindingAdapter("items")
fun setAutoViewTypeAdapterItems(
    recyclerView: RecyclerView,
    items: List<RecyclerViewItem>?
) {

    var adapter: AutoViewTypeRecyclerViewAdapter? =
        recyclerView.adapter as? AutoViewTypeRecyclerViewAdapter

    if (adapter == null) {
        adapter = AutoViewTypeRecyclerViewAdapter()
            .apply { setHasStableIds(false) }

        recyclerView.adapter = adapter
    }

    adapter.updateData(items ?: emptyList())
}