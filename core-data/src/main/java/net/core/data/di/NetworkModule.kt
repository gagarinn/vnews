package net.core.data.di

import dagger.Module
import dagger.Provides
import net.core.data.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

/**
 * Created by Burmaka V on 12.04.2020.
 */
@Module(includes = [RetrofitBuilderModule::class])
interface NetworkModule

@Module(includes = [OkHttpModule::class])
internal class RetrofitBuilderModule {
    @Provides
    @Singleton
    fun providesRetrofit(okHttpClient: OkHttpClient): Retrofit {
        val builder = Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(MoshiConverterFactory.create())
        return builder.build()

    }
}

@Module
internal class OkHttpModule {

        val httpLoggingInteceptor = HttpLoggingInterceptor().also { it.level = HttpLoggingInterceptor.Level.BODY }

    @Singleton
    @Provides
    fun providesOkHttpClient() = OkHttpClient().newBuilder()
        .connectTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS)
//        .addInterceptor(httpLoggingInteceptor)
        .build().also {
            it.readTimeoutMillis
        }
}