package net.vnews.base.di

import android.content.Context
import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.android.AndroidInjectionModule
import dagger.android.ContributesAndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.multibindings.IntoMap
import net.core.data.di.NetworkModule
import net.core.ui.MainActivityViewModel
import net.core.ui.di.CoreUiModule
import net.core.ui.di.ViewModelKey
import net.news.data.di.NewsDataModule
import net.news.ui.di.NewsUiModule
import net.vnews.base.MainActivity
import net.vnews.base.VnewsApplication
import javax.inject.Singleton

/**
 * Created by Burmaka V on 12.04.2020.
 */

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        BaseActivityModule::class,
        CoreUiModule::class,
        NetworkModule::class,
        NewsDataModule::class,
        NewsUiModule::class
    ]
)
interface ApplicationComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(context: Context): Builder

        fun build(): ApplicationComponent
    }

    fun inject(application: VnewsApplication)
}

@Module
abstract class BaseActivityModule {
    @ContributesAndroidInjector(modules = [MainActivityViewModelModule::class])
    abstract fun providesMainActivity(): MainActivity
}

@Module
internal interface MainActivityViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    fun postMainActivityViewModel(viewModel: MainActivityViewModel): ViewModel
}
