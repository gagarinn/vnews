package net.vnews.base

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import net.vnews.base.di.ApplicationComponent
import net.vnews.base.di.DaggerApplicationComponent
import javax.inject.Inject

/**
 * Created by Burmaka V on 10.04.2020.
 */
class VnewsApplication : Application(), HasAndroidInjector {

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>
    lateinit var appComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        val appComponent = DaggerApplicationComponent.builder().application(this).build()
        appComponent.inject(this)
    }
}
