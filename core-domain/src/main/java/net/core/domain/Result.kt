package net.core.domain

/**
 * Created by Burmaka V on 10.04.2020.
 */
sealed class ResultObj<out T: Any> {
    data class Success<out T : Any>(val data: T) : ResultObj<T>()
    data class Error(val exception: Exception) : ResultObj<Nothing>()
}
