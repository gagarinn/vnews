package net.news.domain.model

/**
 * Created by Burmaka V on 09.04.2020.
 */
data class KeyWord(
    val _id: String,
    val kernelIdentifier: String,
    val text: String,
    val __v: String)