package net.news.domain.model

/**
 * Created by Burmaka V on 13.04.2020.
 */
data class Kernel(
    val _id: String,
    val __v: String,
    val date: String
)