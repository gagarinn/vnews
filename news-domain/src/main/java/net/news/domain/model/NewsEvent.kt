package net.news.domain.model

/**
 * Created by Burmaka V on 09.04.2020.
 */
data class NewsEvent(
    val _id: String,
    val hash: String,
    val uri: String,
    val text: String,
    val date: String,
    var metaData: NewsMetaData? = null
)

data class NewsMetaData(
    val imgUrl: String,
    val description: String
)