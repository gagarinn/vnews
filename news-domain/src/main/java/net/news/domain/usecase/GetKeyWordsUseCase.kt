package net.news.domain.usecase

import kotlinx.coroutines.flow.Flow
import net.core.domain.ResultObj
import net.news.domain.model.KeyWord

/**
 * Created by Burmaka V on 09.04.2020.
 */
interface GetKeyWordsUseCase {
    suspend fun execute(kernelId: String): ResultObj<List<KeyWord>>
}