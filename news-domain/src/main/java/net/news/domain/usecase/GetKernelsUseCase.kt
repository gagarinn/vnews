package net.news.domain.usecase

import kotlinx.coroutines.flow.Flow
import net.core.domain.ResultObj
import net.news.domain.model.Kernel

/**
 * Created by Burmaka V on 09.04.2020.
 */
interface GetKernelsUseCase {
    fun execute(): Flow<ResultObj<List<Kernel>>>
}