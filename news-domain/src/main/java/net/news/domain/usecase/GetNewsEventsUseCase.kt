package net.news.domain.usecase

import kotlinx.coroutines.flow.Flow
import net.core.domain.ResultObj
import net.news.domain.model.NewsEvent

/**
 * Created by Burmaka V on 09.04.2020.
 */
interface GetNewsEventsUseCase {
    suspend fun execute(kernelId: String): ResultObj<List<NewsEvent>>
}